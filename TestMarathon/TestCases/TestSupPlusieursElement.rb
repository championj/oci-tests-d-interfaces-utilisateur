#{{{ Marathon
require_fixture 'default'
#}}} Marathon

def test
    java_recorded_version = '1.8.0_60'

    with_window("Saisi") {
        select("Prenom", "mack")
        select("Identifiant", "jhon")
        select("JTextField_2", "111-AAA-222")
        keystroke('JTextField_2', 'Enter')
        click("Ajouter")
        select("Prenom", "cl�ment")
        select("Identifiant", "jack")
        select("JTextField_2", "111-AAA-222")
         keystroke('JTextField_2', 'Enter')
        click("Ajouter")
       
        assert_p("TableItems_0", "RowCount", "2", "{0, Nom}")
        select("TableItems_0", "rows:[0],columns:[Nom]")
        click('TableItems_0')
        click("Delete")
        select("TableItems_0", "rows:[0],columns:[Nom]")
        assert_p("TableItems_0", "RowCount", "1", "{0, Nom}")
      
    }


end
