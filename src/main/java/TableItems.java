import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static javax.swing.ListSelectionModel.SINGLE_SELECTION;

/**
 * create JTable
 *
 * @author Johan and Anthony
 */
public class TableItems extends JTable {

    TableModel tm;


    /**
     * create table
     *
     */
    public TableItems() {


        tm = new TableModel();
        this.setSelectionMode(SINGLE_SELECTION);
        this.setModel(tm);
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1){
                    Main.button2.setEnabled(true);
                }
            }
        });

    }

    /**
     * update abstract table model, when adding new value
     */
    public void update(){
        tm.update();
    }

    /**
     * select a row from a part of the camembert
     *
     * @param index the row select
     */
    public void select(int index){
        ListSelectionModel lsm = this.getSelectionModel();
        if(index==-1) {
            lsm.clearSelection();
        }
        else {
            lsm.setSelectionInterval(index, index);
        }
    }
}
