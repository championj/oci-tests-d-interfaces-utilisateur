/**
 * implementation camembert part
 * @author Johan
 */
public class Item {
	
	private String name;
	private String identifiant;
	private String prenom;

    /**
     * constructor
     *
     * @param name name
     * @param identifiant identifiant
     * @param Prenom prenom
     */
	public Item(String name, String identifiant, String Prenom) {
		super();
		this.name = name;
		this.identifiant = identifiant;
		this.prenom = Prenom;
	}

    /**
     * get name
     *
     * @return name
     */
	public String getName() {
		return name;
	}


    /**
     * set name
     *
     * @param name name
     */
	public void setName(String name) {
		this.name = name;
	}

    /**
     * get identifiant
     *
     * @return identifiant
     */
	public String getIdentifiant() {
		return identifiant;
	}


    /**
     * set identifiant
     *
     * @param identifiant identifiant
     */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}


    /**
     * get prenom
     *
     * @return prenom
     */
	public String getPrenom() {
		return prenom;
	}

    /**
     * set prenom
     *
     * @param prenom prenom
     */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}