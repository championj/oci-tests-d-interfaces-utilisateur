import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Table model for the JTable
 *
 * @author Johan and Anthony
 */
public class TableModel extends AbstractTableModel{


    /**
     * names of the columns of the table
     */
    private final String[] top = {"Nom", "Prenom", "Identifiant"};

    /**
     * constructor
     */
    public TableModel(){
        super();
    }

    /**
     * count the number items
     * @return number of rows
     */
    public int getRowCount() {
        return Main.getItemList().size();
    }

    /**
     * size of top array
     * @return number of columns
     */
    public int getColumnCount() {
        return top.length;
    }

    /**
     * find cell content to create table
     *
     * @param rowIndex row number
     * @param columnIndex column number
     * @return object at the cell[columnIndex][rowIndex]
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        List litem = Main.getItemList();
        Item item = (Item) litem.get(rowIndex);
        switch(columnIndex){
            case 0:
                return item.getName();
            case 1:
                return item.getPrenom();
            case 2:
                return item.getIdentifiant();
            default:
                return null;
        }
    }

    /**
     * column name
     * @param columnIndex column number
     * @return name of the column[columnIndex]
     */
    @Override
    public String getColumnName(int columnIndex){
        return top[columnIndex];
    }

    /**
     * Extend size to display item
     */
    public void update(){
        int i = Main.getItemList().size()-1;
        this.fireTableRowsInserted(i,i);
    }
}
