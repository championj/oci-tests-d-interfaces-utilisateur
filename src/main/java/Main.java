import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * main class
 *
 * @author Johan
 */
public class Main {

    private static ArrayList<Item> itemList = new ArrayList<Item>();
    private static JTextField textArea;
    private static JTextField textArea2;
    private static JTextField textArea3;
    private static TableItems tableau;
    private static JButton button;
    public static JButton button2;

    /**
     * entry of programme
     *
     * @param args argument line command not used
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Saisi");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        panel.add(new JLabel("Nom"));
        textArea = new JTextField();
        textArea.setPreferredSize(new Dimension(200, 25));
        textArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {}
            public void keyPressed(KeyEvent e) {}
            public void keyReleased(KeyEvent e) {
                Main.activeAdd();
            }
        });
        panel.add(textArea);
        panel.add(new JLabel("Prenom"));
        textArea2 = new JTextField();
        textArea2.setPreferredSize(new Dimension(200, 25));
        textArea2.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {}
            public void keyPressed(KeyEvent e) {}
            public void keyReleased(KeyEvent e) {
                Main.activeAdd();
            }
        });
        panel.add(textArea2);
        panel.add(new JLabel("Identifiant"));
        textArea3 = new JTextField();
        textArea3.setPreferredSize(new Dimension(200, 25));
        textArea3.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {}
            public void keyPressed(KeyEvent e) {}
            public void keyReleased(KeyEvent e) {
                Main.activeAdd();
            }
        });
        panel.add(textArea3);
        button = new JButton(new AddAction());
        button.setEnabled(false);
        panel.add(button);
        panel.setSize(450, 250);
        panel2.add(panel, BorderLayout.LINE_START);
        button2 = new JButton(new DelAction());
        panel.add(button2);
        button2.setEnabled(false);
        panel.setSize(450, 250);
        panel2.add(panel, BorderLayout.LINE_START);

        tableau = new TableItems();
        JScrollPane table = new JScrollPane(tableau);
        Dimension d2 = new Dimension(350, 250);
        table.setPreferredSize(d2);
        panel2.add(table, BorderLayout.LINE_END);


        frame.setContentPane(panel2);
        frame.setSize(800, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static List<Item> getItemList() {
        return itemList;
    }

    private static class AddAction extends AbstractAction {
        private AddAction() {
            super("Ajouter");
        }

        public void actionPerformed(ActionEvent e) {
            itemList.add(new Item(textArea.getText(), textArea3.getText(), textArea2.getText()));
            System.out.println(itemList.size());
            tableau.update();
            textArea.setText("");
            textArea2.setText("");
            textArea3.setText("");
            button.setEnabled(false);
        }
    }

    private static void activeAdd(){
        if (!(textArea.getText().equals("") || textArea2.getText().equals("") ||
            textArea3.getText().equals("")) && verifier()) {
        button.setEnabled(true);
        }
        else {
            button.setEnabled(false);
        }

    }

    private static Boolean verifier() {
        Pattern pattern = Pattern.compile("^[0-9]{3}-[A-Z]{3}-[0-9]{3}$");
        Matcher matcher = pattern.matcher(textArea3.getText());
        return matcher.matches();
    }

    private static class DelAction extends AbstractAction {
        private DelAction() {
            super("Delete");
        }

        public void actionPerformed(ActionEvent e) {
            itemList.remove(tableau.getSelectedRow());
            tableau.update();
            button2.setEnabled(false);
        }
    }
}
