'''Launcher uses the properties specified here to launch the application'''

#{{{ Fixture Properties
Fixture_properties = {
        'marathon.project.launcher.model' : 'com.jaliansystems.marathonite.launchers.applet.AppletLauncherModel',
        'marathon.applet.file' : '/home/johan/T\xe9l\xe9chargements/abbot-1.2.0/OCI-Swing-1.0-SNAPSHOT.jar',
        'marathon.recorder.namingstrategy' : 'net.sourceforge.marathon.objectmap.ObjectMapNamingStrategy',
        'marathon.application.vm.arguments' : '',
        'marathon.fixture.reuse' : None
    }
#}}} Fixture Properties

'''Default Fixture'''
class Fixture:


    def teardown(self):
        '''Marathon executes this method at the end of test script.'''
        pass

    def setup(self):
        '''Marathon executes this method before the test script.'''
        pass

    def test_setup(self):
        '''Marathon executes this method after the first window of the application is displayed.
        You can add any Marathon script elements here.'''
        pass

fixture = Fixture()
